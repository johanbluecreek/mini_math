# Mini-math

A mini-course on Mathematica.

This is suppose to be a course/reference sheet on Mathematica. The starting point is for that of a complete beginner and focus on understanding the language used.

### To download

Click this link: [https://gitlab.com/johanbluecreek/mini_math/blob/master/notebook.nb](https://gitlab.com/johanbluecreek/mini_math/raw/master/notebook.nb?inline=false)

For the last part we will go through the package `diffgeo.m`. This is not distributed together with the file of this repository, so you have to download it from here:

http://people.brandeis.edu/~headrick/Mathematica/

If that link does not work, it has also been archived on the Internet Archive Wayback Machine:

https://web.archive.org/web/20190301000000*/http://people.brandeis.edu/~headrick/Mathematica/diffgeo.m

## Retroactive plan (2019)

### 2019-10-11

1 hour: Discussion on the administration of the course. Covered the topics under the headline '*Calculator, "%", and constants*'. End of lesson is marked by "End of Lesson 1" in the notebook.

### 2019-10-18

1 hour: Covered topics until, and including, '*Equal signs*'. End of lesson is marked by "End of Lesson 2" in the notebook.

### 2019-10-24

1 hour: Covered topics under '*Functions (and loops)*'. End of lesson marked by "End of Lesson 3" in the notebook.

### 2019-10-31

1 hour: Covered topics until, and including, '*Algebraic equations (Solve[])*'. End of lesson marked by "End of Lesson 4" in the notebook.

### 2019-11-07

1 hour: Covered topic until '*Making your own functions*'. End of lesson marked by "End of Lesson 5" in the notebook.

### 2019-11-15

1 hour: Covered topics until '*Blanks (_) and methods*'. End of lesson marked by "End of Lesson 6" in the notebook.

### 2019-12-06

1 hour: Covered topic until '*Nilpotent fields*'. End of lesson marked by "End of Lesson 7" in the notebook.

### 2020-01-10

1 hour: Covered the topic '*Making your own operator*'. End of lesson marked by "End of Lesson 8" in the notebook.

### 2020-01-17

1 hour: ...
